﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car;

public class MainUI : MonoBehaviour
{
    private bool paused = false;
    
    public RectTransform pausePnl;
    public RectTransform pauseBtn;
    public RectTransform speedScrBr;
    public RectTransform speedTxt;

    public CarController carCntrlr;
    public GameObject launchPoint;

    public GameManager manager;
    
    public void OnRestartBtn()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Game");
    }

    public void OnMenuBtn()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Start");
    }

    public void OnPauseBtn()
    {
        paused = !paused;
        handlePauseMenu(paused);
    }
    
    public void OnLaunchBtn()
    {
        manager.SpawnGrenade(launchPoint);
    }

        
    void handlePauseMenu(bool paused)
    {
        Debug.Log(" paused " + paused);
        pausePnl.gameObject.SetActive(paused);
        pauseBtn.gameObject.SetActive(!paused);
        Time.timeScale = paused ? 0f : 1f;
    }
    
    void Update()
    {
        if (paused)
        {
            return;
        }
        
        var speedValue = carCntrlr.CurrentSpeed / carCntrlr.MaxSpeed;
        
        var scrlBar = speedScrBr.gameObject.GetComponent<Scrollbar>();
        scrlBar.size = speedValue;

        var text = speedTxt.gameObject.GetComponent<Text>();
        text.text = carCntrlr.CurrentSpeed.ToString("####000.00");
        
        
    }
}