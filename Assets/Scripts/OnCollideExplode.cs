﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollideExplode : MonoBehaviour
{
	public GameManager explodeSpawner;
	
	private void OnCollisionEnter(Collision other)
	{
		var rigid = gameObject.GetComponent<Rigidbody>();
		explodeSpawner.SpawnExplosion(rigid, other.contacts[0].point);
		Destroy(this.gameObject);
	}
}