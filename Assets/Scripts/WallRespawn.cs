﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WallRespawn : MonoBehaviour
{
	
	private bool destructed = false;
	private List<GameObject> gameObjects;
	private List<Renderer> renderers;

	public System.Action<GameObject> onNotSeendDestruct;
	
	void Start ()
	{
		destructed = false;
		gameObjects = new List<GameObject>();
		
		var transforms = gameObject.GetComponentsInChildren<Transform>().ToList();
		renderers = transforms.Where(a => a.GetComponent<Renderer>() != null).Select(x=>x.GetComponent<Renderer>()).ToList();
	}

	public void OnChildrenCollisionEvent()
	{
		destructed = true;
	}
	
	void Update ()
	{
		var allNotSeen = renderers.All(a => a.isVisible == false);
		if(allNotSeen)
		{
			if(destructed)
			{
				onNotSeendDestruct(this.gameObject);	
			}
		}
	}
}
