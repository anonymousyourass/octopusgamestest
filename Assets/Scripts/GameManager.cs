﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject smallBoxesPrefab;
    public GameObject grenadePrefab;
    public GameObject explosionPrefab;

    private float maxBorder = 485f;

    public void SpawnSmallBoxes(int count)
    {
        for (var i = 0; i < count; i++)
        {
            SpawnBox();
        }
    }

    public void SpawnBox()
    {
        var go = GameObject.Instantiate(smallBoxesPrefab, Vector3.zero, Quaternion.identity);

        go.name = Time.realtimeSinceStartup.ToString();

        var randomX = UnityEngine.Random.Range(-maxBorder, maxBorder);
        var randomZ = UnityEngine.Random.Range(-maxBorder, maxBorder);

        go.transform.position = new Vector3(randomX, 0f, randomZ);
        var wr = go.GetComponent<WallRespawn>();

        wr.onNotSeendDestruct = (g) =>
        {
            SpawnBox();
            Destroy(wr.gameObject);
        };
    }

    public float grenadeSpeed = 100f; 
    
    public void SpawnGrenade(GameObject launchPoint)
    {
        var grenade = Instantiate(grenadePrefab, launchPoint.transform.position, launchPoint.transform.rotation);
        var body = grenade.GetComponent<Rigidbody>();
        body.AddForce(grenade.transform.forward * grenadeSpeed, ForceMode.VelocityChange);
        grenade.gameObject.GetComponent<OnCollideExplode>().explodeSpawner = this;
    }
    
    public void SpawnExplosion(Rigidbody body, Vector3  point)
    {
        Instantiate(explosionPrefab, point, Quaternion.identity);
    }

    void Start()
    {
        SpawnSmallBoxes(20);
    }
}