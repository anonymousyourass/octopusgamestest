﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollisionParentNotifier : MonoBehaviour
{
    public const int LayerPlayer = 9;
    public const int LayerExplosion = 10;

    private WallRespawn wallRespawn;

    void Start()
    {
        wallRespawn = transform.root.GetComponent<WallRespawn>();
    }

    private void OnExplosion()
    {
        if (wallRespawn != null)
        {
            wallRespawn.OnChildrenCollisionEvent();
        }   
    }
    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == LayerPlayer || other.gameObject.layer == LayerExplosion)
        {
            if (wallRespawn != null)
            {
                wallRespawn.OnChildrenCollisionEvent();
            }    
        }
    }
}