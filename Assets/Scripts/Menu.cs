﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void OnStartBtn()
    {
        SceneManager.LoadScene("Game");
    }

    public void OnExitBtn()
    {
        Application.Quit();
    }
}